<?php
class Database {

    public $HTML_OUTPUT_MIN = 0;
    public $HTML_OUTPUT_DEFAULT = 1;
    public $HTML_OUTPUT_FULL = 2;

    private $db_user;
    private $db_pass;
    private $db_address;
    private $db_name;
    private $con = false;
    private $myconn;
    private $result = array();

    public function __construct($DB_ADDRESS, $DB_USER, $DB_PASS, $DB_NAME) {
        $this->db_address = $DB_ADDRESS;
        $this->db_user = $DB_USER;
        $this->db_pass = $DB_PASS;
        $this->db_name = $DB_NAME;
    }

    public function connect() {
        if(!$this->con){
            $this->myconn = new mysqli($this->db_address,
                                       $this->db_user,
                                       $this->db_pass,
                                       $this->db_name);
            if($this->myconn){
                $this->con = true;
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    public function disconnect()    {
        if($this->con){
            if($this->myconn->close()){
                $this->con = false;
                return true;
            } else {
                return false;
            }
        }
    }
    public function createTable($name, $description) {
        $this->myconn->query("DROP TABLE IF EXISTS {$name}");
        $created = $this->myconn->query("CREATE TABLE {$name}({$description})");
        if (!$created) {
            echo "Не удалось создать таблицу: (" . $this->myconn->errno . ") " . $this->myconn->error;
            return false;
        } else {
            return true;
        }
    }
    public function alterTable($table, $changes){
        $alter = "ALTER TABLE {$table} ".$changes;
        $changed = $this->myconn->query($alter);
        if (!$changed) {
            echo "Не удалось изменить таблицу: (" . $this->myconn->errno . ") " . $this->myconn->error;
            return false;
        } else {
            return true;
        }
    }
    public function select($table, $rows = '*', $where = null, $order = null, $sort_direction = null, $limit = null, $offset = null){
        $q = 'SELECT '.$rows.' FROM '.$table;
        if($where != null)
            $q .= ' WHERE '.$where;
        if($order != null)
            $q .= ' ORDER BY '.$order;
        if($sort_direction != null)
            $q .= ' '.$sort_direction.' ';
        if($limit != null)
            $q .= ' LIMIT '.$limit;
        if($offset != null)
            $q .= ' OFFSET '.$offset;
        if($this->tableExists($table)){
            $query = $this->myconn->query($q);
            if($query){
                $this->numResults = mysqli_num_rows($query);
                for($i = 0; $i < $this->numResults; $i++){
                    $r = mysqli_fetch_array($query);
                    $key = array_keys($r);
                    for($x = 0; $x < count($key); $x++){
                        if(!is_int($key[$x])){
                            if(mysqli_num_rows($query) > 1)
                                $this->result[$i][$key[$x]] = $r[$key[$x]];
                            else if(mysqli_num_rows($query) < 1)
                                $this->result = null;
                            else
                                $this->result[$key[$x]] = $r[$key[$x]];
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function insert($table,$values,$rows = null){
        if($this->tableExists($table)){
            $insert = 'INSERT INTO '.$table;
            if($rows != null){
                $insert .= ' ('.$rows.')';
            }
            for($i = 0; $i < count($values); $i++){
                if(is_string($values[$i]))
                    $values[$i] = '"'.$values[$i].'"';
            }
            $values = implode(',',$values);
            $insert .= ' VALUES ('.$values.')';
            $ins = $this->myconn->query($insert);
            if($ins){
                return true;
            } else {
                return false;
            }
        }
    }
    public function delete($table,$where = null){
        if($this->tableExists($table)){
            if($where == null){
                $delete = 'DELETE '.$table;
            } else {
                $delete = 'DELETE FROM '.$table.' WHERE '.$where;
            }
            $del = $this->myconn->query($delete);
            if($del) {
                return true;
            } else {
               return false;
            }
        } else {
            return false;
        }
    }
    public function update($table,$rows,$where,$condition) {
        if($this->tableExists($table)) {
            // Parse the where values
            // even values (including 0) contain the where rows
            // odd values contain the clauses for the row
            for($i = 0; $i < count($where); $i++) {
                if($i%2 != 0) {
                    if(is_string($where[$i])) {
                        if(($i+1) != count($where))
                            $where[$i] = '"'.$where[$i].'" AND ';
                        else
                            $where[$i] = '"'.$where[$i].'"';
                    }
                }
            }
            $where = implode($condition,$where);
            $update = 'UPDATE '.$table.' SET ';
            $keys = array_keys($rows);
            for($i = 0; $i < count($rows); $i++) {
                if(is_string($rows[$keys[$i]])) {
                    $update .= $keys[$i].'="'.$rows[$keys[$i]].'"';
                } else {
                    $update .= $keys[$i].'='.$rows[$keys[$i]];
                }
                // Parse to add commas
                if($i != count($rows)-1) {
                    $update .= ',';
                }
            }
            $update .= ' WHERE '.$where;
            $query = $this->myconn->query($update);
            if($query){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getResult() {
        return $this->result;
    }
    private function tableExists($table) {
        $tablesInDb = $this->myconn->query('SHOW TABLES FROM '.$this->db_name.' LIKE "'.$table.'"');
        if($tablesInDb){
            if(mysqli_num_rows($tablesInDb)==1){
                return true;
            } else {
                return false;
            }
        }
    }
}
?>
