<?php
include 'Database.php';
include 'variables.php';
include 'ResultsFormatter.php';
include 'helper.php';

$table = 'test';

$db = new Database($DB_ADDRESS, $DB_USER, $DB_PASS, $DB_NAME);
$db->connect();
$db->createTable($table, 'id INT, Username nvarchar(20), Description nvarchar(100)');

// Добавляю 100 строк
$rows_number = 100;
for ($i=0; $i <= $rows_number; $i++){
    $db->insert($table,array($i,"Name".$i, "Description".(time()+$i)));
}

// Добавляю колонку после имени
$db->alterTable($table, 'ADD surname nvarchar(50) AFTER Username');

// Меняю рандомное количество строк на рандомные фамилии
$rand_number = rand(1, $rows_number);
$rand_ids = array();
for ( $i=0; $i <= $rand_number; $i++) {
    array_push($rand_ids, rand(1, $rand_number));
}
foreach ($rand_ids as $rand_ids_value){
    $db->update($table,array('surname'=>generateRandomString(10)),array('id',$rand_ids_value),"=");
}

// Вывожу результаты
$db->select($table);
// как CSV
$rf = new ResultsFormatter();
$csv_output = fopen("outputs/output.csv",'w');
foreach($rf->formatAsCSV($db->getResult()) as $csv_line) {
  fputcsv($csv_output, $csv_line);
}

fclose($csv_output);

// как HTML слабый
$html_output = fopen("outputs/output.html", 'w');
fwrite($html_output, $rf->formatAsHTML($db->getResult(), $db->HTML_OUTPUT_MIN));
fclose($html_output);

// как HTML с заголовком
$html_output2 = fopen("outputs/output2.html", 'w');
fwrite($html_output2, $rf->formatAsHTML($db->getResult(), $db->HTML_OUTPUT_DEFAULT));
fclose($html_output2);

// как HTML с заголовком и хедером
$html_output3 = fopen("outputs/output3.html", 'w');
fwrite($html_output3, $rf->formatAsHTML($db->getResult(), $db->HTML_OUTPUT_FULL));
fclose($html_output3);

// в STDOUT (консоль)
$rf->formatToStdout($db->getResult());

// в файл
$txt_output = fopen("outputs/output.txt", 'w');
fwrite($txt_output, $rf->formatToStdout($db->getResult(), 'file'));
fclose($txt_output);

$db->disconnect();

?>
