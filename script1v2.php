<?php
include 'Database.php';
include 'variables.php';

$table = 'test';

$db = new Database($DB_ADDRESS, $DB_USER, $DB_PASS, $DB_NAME);
$db->connect();
$db->createTable($table, 'id INT, Username nvarchar(20), Description nvarchar(100)');

$db->insert($table,array(1,'Name','Just a name'));
$db->insert($table,array(2,'Name2','Just a name2'));
$db->insert($table,array(3,'Name3','Just a name3'));
$db->insert($table,array(4,'Name4','Just a name4'));
$db->insert($table,array(5,'Name5','Just a name5'));

$db->select($table, '*', '', 'id', 'DESC', '5');
$db->getHumanReadableResult();

$db->update($table, array('Username'=>'nordman', 'Description'=>'text text'), array('id', '1'),'=');
$db->update($table, array('Username'=>'bookkk', 'Description'=>'text text2'), array('id','2'),'=');
$db->update($table, array('Username'=>'hoonk', 'Description'=>'null'), array('id','4'),'=');

$db->select($table);
$db->getHumanReadableResult();

$db->disconnect();
?>
