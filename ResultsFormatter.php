<?php
class ResultsFormatter {

    private function headerDecorator($text, $align = 'left') {
        return "<div style='background: #64B5F6;font-size: 20px;padding: 10px;border: 2px solid #000;text-align:{$align}'>{$text}</div>";
    }

    public function formatAsCSV($res) {
        $output = array();
        array_push($output, array_keys($res[0]));
        foreach ($res as $res_item) {
            array_push($output, array_values($res_item));
        }
        return $output;
    }
    public function formatAsHTML($res, $decoration = 0) {
        $html_header_template = '<!DOCTYPE html><html><head><meta charset="utf-8"><meta name="viewport" content="width=device-width">
    <style>.table{width:100%;max-width:100%;margin-bottom:1rem;background-color:transparent}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}.table thead th{vertical-align:bottom;border-bottom:2px solid #dee2e6}.table tbody+tbody{border-top:2px solid #dee2e6}.table .table{background-color:#fff} </style>
    </head><body>';
        $rows_number = count($res);
        $tableheader = '<table class="table"><thead><tr>';
        $tablebody = '';
        foreach (array_keys($res[0]) as $head) {
            $tableheader.= "<td> {$head} </td>";
        }
        $tablebody.='</tr></thead><tbody>';
        foreach ($res as $row) {
            $tablebody.='<tr>';
            foreach ($row as $row_element) {
                $tablebody.="<td>{$row_element}</td>";
            }
            $tablebody.='</tr>';
        }
        $tablebody.='</tbody></table></body></html>';
        if ($decoration == 0) {
            return $html_header_template . $tableheader . $tablebody;
        } elseif ($decoration == 1) {
            return $html_header_template . $this->headerDecorator("Report for SQL request") . $tableheader . $tablebody;
        } elseif ($decoration == 2) {
            return $html_header_template . $this->headerDecorator("Report for SQL request") . $tableheader . $tablebody . $this->headerDecorator("{$rows_number} records", 'right');
        }
    }
    public function formatToStdout($res, $file = false) {
        $output = "\n REQUEST RESULT \n\n";
        $header = array_keys($res[0]);
        $output.=vsprintf(str_repeat("| %s ", count($header)), $header)."\n\n";
        foreach($res as $row) {
            $format = str_repeat("| %s ", count($row));
            $output.=vsprintf($format, $row)."\n";
        }
        if ($file) {
            return $output;
        } else {
            echo $output;
        }
    }
}
?>
